<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
    <form action="EditFlightServlet" method="post">
        <p>ID:</p>
        <input type="text" name="id"/>
        <p>Number:</p>
        <input type="text" name="number"/>
        <p>Airplane type:</p>
        <input type="text" name="airplane-type"/>
        <p>Departure city:</p>
        <input type="text" name="departure-city"/>
        <p>Departure date:</p>
        <input type="text" name="departure-date"/>
        <p>Departure time:</p>
        <input type="text" name="departure-time"/>
        <p>Arrival city:</p>
        <input type="text" name="arrival-city"/>
        <p>Arrival date:</p>
        <input type="text" name="arrival-date"/>
        <p>Arrival time:</p>
        <input type="text" name="arrival-time"/>
        <input type="submit" value="Edit"/>
    </form>
</body>
</html>