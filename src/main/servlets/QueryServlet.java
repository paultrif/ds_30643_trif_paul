package main.servlets;

import jdk.internal.org.xml.sax.InputSource;
import jdk.internal.org.xml.sax.SAXException;
import main.dao.CityDAO;
import main.dao.FlightDAO;
import main.dao.UserDAO;
import main.entities.City;
import main.entities.Flight;
import main.entities.User;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.*;

public class QueryServlet extends HttpServlet {


 
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {



        response.setContentType("text/html");
        PrintWriter printr = response.getWriter();

        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            String username = cookies[0].getValue();
            User user = UserDAO.findUser(username);
            if (!username.equals("") && user.getRole().equals("user")) {

                request.getRequestDispatcher("query.jsp").include(request, response);

                printr.println("<h1>Cities</h1>");

                List<City> cities = CityDAO.findCities();

                printr.println("<table border='1' width='100%'>");
                printr.println("<tr>" +
                        "<th>ID</th>" +
                        "<th>Name</th>" +
                        "<th>Latitude</th>" +
                        "<th>Longitude</th>" +
                        "</tr>");

                for (City ct : cities) {
                    printr.println("<tr>" +
                            "</td><td>" + ct.getId() +
                            "</td><td>" + ct.getName() +
                            "</td><td>" + ct.getLatitude() +
                            "</td><td>" + ct.getLongitude() +
                            "</td>" +
                            "</tr>");
                }

                printr.println("</table>");

                String city_param = request.getParameter("city");
                City city = CityDAO.findCityByName(city_param);


                String queryUrl = "http://www.new.earthtools.org/timezone-1.1/" + city.getLatitude() + "/" + city.getLongitude();

                URL object = new URL(queryUrl);
                HttpURLConnection con = (HttpURLConnection) object.openConnection();

                con.setRequestMethod("GET");

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer buffer_response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    buffer_response.append(inputLine);
                }
                in.close();

                String respStr = buffer_response.toString();

                int start = respStr.indexOf("<localtime>");
                int end = respStr.indexOf("</localtime>");


                printr.println("Local time in "+ city_param + " is " + respStr.substring(start, end));
            }
        }
    }

    private static void getTags(final String str) {
        final Matcher matcher = TAG_REGEX.matcher(str);
        while (matcher.find()) {
            System.out.println(matcher);
        }
        return;
    }

    private static final Pattern TAG_REGEX = Pattern.compile("<tag>(.+?)</tag>", Pattern.DOTALL);

    private static List<String> getValues(final String str) {
        final List<String> tags = new ArrayList<String>();
        final Matcher matcher = TAG_REGEX.matcher(str);
        while (matcher.find()) {
            tags.add(matcher.group(1));
        }
        return tags;
    }

}
