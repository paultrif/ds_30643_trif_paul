package main.servlets;

import main.dao.FlightDAO;
import main.dao.UserDAO;
import main.entities.Flight;
import main.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ViewUserFlightsServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter printr = response.getWriter();

        Cookie cooks[] = request.getCookies();
        if (cooks != null) {
            String username = cooks[0].getValue();
            User user = UserDAO.findUser(username);
            if (!username.equals("") && user.getRole().equals("user")) {
                request.getRequestDispatcher("user.jsp").include(request, response);

                printr.println("<h1>Flights</h1>");

                List<Flight> flights = FlightDAO.findFlights();

                printr.println("<table border='1' width='100%'>");
                printr.println("<tr>" +
                        "<th>ID</th>" +
                        "<th>Number</th>" +
                        "<th>Airplane type</th>" +
                        "<th>Departure city</th>" +
                        "<th>Departure date</th>" +
                        "<th>Departure time</th>" +
                        "<th>Arrival city</th>" +
                        "<th>Arrival date</th>" +
                        "<th>Arrival time</th>" +
                        "</tr>");

                for (Flight flight : flights) {
                    printr.println("<tr>" +
                            "<td>" + flight.getId() +
                            "</td><td>" + flight.getNumber() +
                            "</td><td>" + flight.getAirplaneType() +
                            "</td><td>" + flight.getDepartureCity().getName() +
                            "</td><td>" + flight.getDepartureDate() +
                            "</td><td>" + flight.getDepartureTime() +
                            "</td><td>" + flight.getArrivalCity().getName() +
                            "</td><td>" + flight.getArrivalDate() +
                            "</td><td>" + flight.getArrivalTime() +
                            "</td>" +
                            "</tr>");
                }

                printr.println("</table>");

            } else if (!username.equals("") && user.getRole().equals("admin")) {
                request.getRequestDispatcher("admin.jsp").include(request, response);
                printr.println("<br/>Your are not an admin!");
            } else {
                printr.println("You need to login first!");
                request.getRequestDispatcher("index.html").include(request, response);
            }
        }

    }

}
