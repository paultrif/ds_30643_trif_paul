package main.servlets;

import main.dao.UserDAO;
import main.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {


    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request,response);
        return;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String username = request.getParameter("username");
        String password = request.getParameter("password");


        if( username == ""){
            request.getRequestDispatcher("index.html").include(request, response);
            out.println("Insert a username");
        }

        else if (password == ""){
            request.getRequestDispatcher("index.html").include(request, response);
            out.println("Insert a password");

        }

        else {

            User user = UserDAO.findUser(username);

            if (password.equals(user.getPassword())) {
                Cookie cook = new Cookie("name", username);
                response.addCookie(cook);
                request.setAttribute("loggedUser", user.getUsername());
                HttpSession session = request.getSession(true);
                session.setAttribute("loggedUser", user);
                if (user.getRole().equals("admin")) {
                    request.getRequestDispatcher("admin.jsp").include(request, response);
                } else if (user.getRole().equals("user")) {
                    request.getRequestDispatcher("user.jsp").include(request, response);
                }
            } else {
                out.print("Incorrect username or password!");
                request.getRequestDispatcher("index.html").include(request, response);
            }

        }

    }


}
