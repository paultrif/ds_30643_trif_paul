package main.servlets;

import main.dao.CityDAO;
import main.dao.FlightDAO;
import main.dao.UserDAO;
import main.entities.City;
import main.entities.Flight;
import main.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddFlightServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        PrintWriter printr = response.getWriter();
        response.setContentType("text/html");
        request.setAttribute("loggedUser", "admin");
        HttpSession session = request.getSession(true);
        session.setAttribute("loggedUser", "admin");
        Cookie cooks[] = request.getCookies();
        if (cooks != null) {
            String username = cooks[0].getValue();
            User user = UserDAO.findUser(username);
            if (!username.equals("") && user.getRole().equals("admin")) {
                request.getRequestDispatcher("admin.jsp").include(request, response);

                String nr = request.getParameter("number");
                String planeType = request.getParameter("airplane-type");

                String departCity = request.getParameter("departure-city");
                City departureCity = CityDAO.findCityById(Integer.parseInt(departCity));


                String departDate = request.getParameter("departure-date");
                String departureTimeString = request.getParameter("departure-time");

                String arrivCity = request.getParameter("arrival-city");
                City arrivalCity = CityDAO.findCityById(Integer.parseInt(arrivCity));

                String arrivalDateString = request.getParameter("arrival-date");
                String arrivalTimeString = request.getParameter("arrival-time");

                Date departureDate = null;
                Time departureTime = null;
                Time arrivalTime = null;
                Date arrivalDate = null;

                try {
                    departureDate = new SimpleDateFormat("yyyy-MM-dd").parse(departDate);
                    arrivalDate = new SimpleDateFormat("yyyy-MM-dd").parse(arrivalDateString);
                    departureTime = new Time(new SimpleDateFormat("HH:mm:ss").parse(departureTimeString).getTime());
                    arrivalTime = new Time(new SimpleDateFormat("HH:mm:ss").parse(arrivalTimeString).getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Flight fli = new Flight();


                fli.setDepartureCity(departureCity);
                fli.setNumber(nr);
                fli.setAirplaneType(planeType);

                fli.setDepartureDate(departureDate);
                fli.setArrivalCity(arrivalCity);

                fli.setDepartureTime(departureTime);
                fli.setArrivalDate(arrivalDate);
                fli.setArrivalTime(arrivalTime);

                FlightDAO.addFlight(fli);

                response.sendRedirect("ViewFlightsServlet");
            } else if (!username.equals("") && user.getRole().equals("user")) {
                request.getRequestDispatcher("user.jsp").include(request, response);
                printr.println("<br/>You are not an admin!");
            } else {
                printr.println("You need to login first!");
                request.getRequestDispatcher("index.html").include(request, response);
            }
        }

        printr.close();

    }

}
