package main.servlets;

import main.dao.CityDAO;
import main.dao.FlightDAO;
import main.dao.UserDAO;
import main.entities.City;
import main.entities.Flight;
import main.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EditFlightServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter printr = response.getWriter();

        Cookie coocks[] = request.getCookies();
        if (coocks != null) {
            String username = coocks[0].getValue();
            User user = UserDAO.findUser(username);
            if (!username.equals("") && user.getRole().equals("admin")) {
                request.getRequestDispatcher("admin.jsp").include(request, response);

                String sid = request.getParameter("id");

                request.setAttribute("loggedUser", "admin");
                HttpSession session = request.getSession(true);
                session.setAttribute("loggedUser", "admin");


                String number = request.getParameter("number");
                String airplaneType = request.getParameter("airplane-type");

                String departName = request.getParameter("departure-city");
                City departureCity = CityDAO.findCityById(Integer.parseInt(departName));

                String departDateString = request.getParameter("departure-date");
                String departTimeString = request.getParameter("departure-time");

                String arrivName = request.getParameter("arrival-city");
                City arrivalCity = CityDAO.findCityById(Integer.parseInt(arrivName));

                String arrivDateString = request.getParameter("arrival-date");
                String arrivTimeString = request.getParameter("arrival-time");

                int id = Integer.parseInt(sid);

                Date departDate = null;
                Date arrivDate = null;
                Time departTime = null;
                Time arrivTime = null;

                try {
                    departDate = new SimpleDateFormat("yyyy-MM-dd").parse(departDateString);
                    arrivDate = new SimpleDateFormat("yyyy-MM-dd").parse(arrivDateString);
                    departTime = new Time(new SimpleDateFormat("HH:mm:ss").parse(departTimeString).getTime());
                    arrivTime = new Time(new SimpleDateFormat("HH:mm:ss").parse(arrivTimeString).getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Flight flight = new Flight();

                flight.setId(id);

                if(number != "")
                    flight.setNumber(number);

                if(airplaneType != "")
                     flight.setAirplaneType(airplaneType);

                if(departureCity != null)
                     flight.setDepartureCity(departureCity);

                if(departDate != null)
                     flight.setDepartureDate(departDate);

                if(departTime != null)
                     flight.setDepartureTime(departTime);

                if(arrivalCity != null)
                     flight.setArrivalCity(arrivalCity);

                if(arrivDate != null)
                     flight.setArrivalDate(arrivDate);

                if(arrivTime != null)
                     flight.setArrivalTime(arrivTime);

                FlightDAO.editFlight(flight);

                response.sendRedirect("ViewFlightsServlet");
            } else if (!username.equals("") && user.getRole().equals("user")) {
                request.getRequestDispatcher("user.jsp").include(request, response);
                printr.println("<br/>You don't have enough privileges!");
            } else {
                printr.println("Please login first!");
                request.getRequestDispatcher("index.html").include(request, response);
            }
        }

        printr.close();

    }

}
